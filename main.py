from flask import Flask, render_template, redirect, url_for, request, session
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import EncryptedType
from sqlalchemy.schema import PrimaryKeyConstraint
from sqlalchemy.orm import backref, relationship
from wtforms import StringField, PasswordField, SelectField, SubmitField, IntegerField, FloatField, HiddenField, MultipleFileField, FileField
from wtforms.validators import InputRequired, Email, Length, NumberRange, DataRequired
from wtforms.widgets import TextArea
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import uuid
import os
from threading import Thread

# Initializing Flask-app and configuring it to connect to the SQL database
app = Flask(__name__)
app.config['SECRET_KEY'] = 'OA%/$J@gd5VGFEK'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:!compsci1234@localhost:3306/classtracker'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['SESSION_SQLALCHEMY_TABLE'] = 'sessions'

bootstrap = Bootstrap(app)
socketio = SocketIO(app, async_mode="threading")

# Setting up SQLAlchemy
db = SQLAlchemy(app)

app.config['SESSION_SQLALCHEMY'] = db


# Setting up the login manager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

# Classes using SQLAlchemy, each class represents a table in the SQL database
# Creating a class for the user login information
class User(UserMixin, db.Model):
    userId = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))

    # Overriding get_id to return UserId
    def get_id(self):
        return self.userId

# Creating a class to hold classroom information
class Classroom(db.Model):
    num = db.Column(db.Integer, autoincrement=True, primary_key=True)
    classId = db.Column(db.String(6))
    owner = db.Column(db.Integer)
    name = db.Column(db.String(250))
    desc = db.Column(db.String(250))
    #lectures = relationship("Classroom", backref=backref("items", cascade="all, delete-orphan"))

    def to_dict(self):
        return {'classId' : self.classId, 'owner' : self.owner, 'name' : self.name, 'desc' : self.desc}

# Creating a class to hold individual lecture information
class Lecture(db.Model):
    num = db.Column(db.Integer, primary_key=True, autoincrement=True)
    lectureId = db.Column(db.String(6))
    classId = db.Column(db.String(6))
    name = db.Column(db.String(250))
    desc = db.Column(db.String(250))
    #parent = relationship(Classroom, backref=backref("children", cascade="all,delete"))

# Creating a class to hold the topics discussed in each lecture
class Topic(db.Model):
    topicId = db.Column(db.Integer, primary_key=True, autoincrement=True)
    lectureId = db.Column(db.String(6))
    date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    name = db.Column(db.String(250))
    topicDescription = db.Column(db.String(250))
    ratingSum = db.Column(db.Integer, default=1)
    ratingTot = db.Column(db.Integer, default=1)

    #parent = relationship(Lecture, backref=backref("children", cascade="all,delete"))

db.create_all()
db.session.commit()

@login_manager.user_loader
def load_user(userId):
    return User.query.get(userId)

# Form classes using WTForms, these classes can be injected into .html templates
# as HTML <form>
# Form for logging the user in
class LoginForm(FlaskForm):
    email = StringField("Email", validators=[InputRequired(), Email(message="Please enter a valid email")])
    password = PasswordField("Password", validators=[InputRequired()])
    submit = SubmitField("Login")

# Form for registering a new user
class RegistrationForm(FlaskForm):
    email = StringField("Email", validators=[InputRequired(), Email(message="Please enter a valid email")])
    password = PasswordField("Password", validators=[InputRequired()])
    submit = SubmitField("Create Account")

# Form to join a live lecture
class JoinLectureForm(FlaskForm):
    lecture_id = StringField("Lecture Code", validators=[InputRequired()])
    submit = SubmitField("Join Lecture")

# Form to create a lecture
class LectureForm(FlaskForm):
    lecture_data = HiddenField("lecture_data")
    name = StringField("Lecture Name", validators=[InputRequired()])
    description = StringField("Lecture Description", widget=TextArea(), validators=[InputRequired()])

# Form to create a Class
class ClassForm(FlaskForm):
    class_data = HiddenField("class_data")
    name = StringField("Class Name", validators=[InputRequired()])
    description = StringField("Class Description", widget=TextArea(), validators=[InputRequired()])

# Form to Create a Topic
class TopicForm(FlaskForm):
    topic_data = HiddenField("topic_data")
    name = StringField("Topic Name", validators=[InputRequired()])
    description = StringField("Topic Description", widget=TextArea(), validators=[InputRequired()])
    submit = SubmitField("Craete Topic")
# form for rating topics
class RatingForm(FlaskForm):
    topic_data = HiddenField('topic_data')
    rating =  HiddenField('rating')
    lecture_id = HiddenField('lecture_id')


@socketio.on('vote')
def vote(msg):
    print("id: " + str(msg['id']), "vote: " + str(msg['vote']))

    t = Topic.query.filter_by(topicId=msg['id']).first();
    id = str(msg['id'])

    if t is not None:
        join_room(t.lectureId)

        if id in session:
            if session[id] != str(msg['vote']):
                if session[id] == '1' and str(msg['vote']) == '0':
                    t.ratingSum = t.ratingSum - 1;
                else :
                    t.ratingSum = t.ratingSum + int(msg['vote']);
                socketio.emit('vote_response', {'id' : msg['id'], 'index' : msg['index'], 'sum' : t.ratingSum, 'total' : t.ratingTot}, room=t.lectureId)
                db.session.commit();
                session[id] = str(msg['vote'])
        else:
            t.ratingTot = t.ratingTot + 1;
            t.ratingSum = t.ratingSum + int(msg['vote']);
            socketio.emit('vote_response', {'id' : msg['id'], 'index' : msg['index'], 'sum' : t.ratingSum, 'total' : t.ratingTot}, room=t.lectureId)
            db.session.commit();
            session[id] = str(msg['vote'])

@socketio.on('topic_reset')
def topic_reset(msg):
    print('Recieved reset call for: ' + str(msg['id']))
    Topic.query.filter_by(topicId=msg['id']).update(dict(ratingSum=1, ratingTot=1))
    db.session.commit()

@app.route('/liveupdate', methods=['GET', 'POST'])
def liveupdate():
    return render_template('liveupdate.html')

# Beginning of flask : Routing
@app.route("/", methods=['GET', 'POST'])
@app.route("/home", methods=['GET'])
def index():
    form = JoinLectureForm()
    navs = {'home': True, 'login': False, 'register': False, 'help': "home"};
    if current_user.is_authenticated:
        navs = {'home': True, 'classes': False, 'logout': False, 'help': "home"};

    if request.method == 'POST':
        # If the form validates
        if form.validate_on_submit():
            # Getting email and password from form
            lecture_id = form.lecture_id.data
            # Checking if lecture exists
            if db.session.query(db.exists().where(lecture.id == lecture_id)).scalar():
                return redirect(url_for('lecture'), lecture_id=lecture_id)

    return render_template("index.html", form=form, navs=navs)


@app.route("/register", methods=['GET', 'POST'])
def register():

    form = RegistrationForm()

    # If method is POST
    if request.method == 'POST':
        # If the form validates
        if form.validate_on_submit():
            # Getting email and password from form
            email = form.email.data
            password = generate_password_hash(form.password.data, method='sha256')
            # Checking if user exists
            if db.session.query(db.exists().where(User.email == email)).scalar():
                return render_template("registration.html", form=form)

            # Creating new user
            new_user = User(email=email, password=password)

            # Commiting user to database
            db.session.add(new_user)
            db.session.commit()
            print("User added to database")
            # redirecting to login
            return redirect(url_for('login'))

    navs = {'home': False, 'login': False, 'register': True, 'help': "register"};

    return render_template("registration.html", form=form, navs=navs)


@app.route("/login", methods=['GET', 'POST'])
def login():

    form = LoginForm()

    # If the form is filled out and submitted properly, it will validate
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('classes'))

    navs = {'home': False, 'login': True, 'register': False, 'help': "login"};

    return render_template("login.html", form=form, navs=navs)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))

@app.route("/classes", methods=['GET', 'POST'])
@login_required
def classes():

    form = ClassForm()

    if form.validate_on_submit():
        if form.class_data.data != "":
            Classroom.query.filter_by(classId=form.class_data.data).update(dict(name=form.name.data, desc=form.description.data))
            db.session.commit()
        else:
            # Generating class code
            code = str(uuid.uuid4())[:6]
            # While there exists a class with the generated class code, generate a new code
            while db.session.query(db.exists().where(Classroom.classId == code)).scalar():
                code = str(uuid.uuid4())[:6]

            # Creating new class
            new_class = Classroom(classId=code, owner=current_user.userId, name=form.name.data, desc=form.description.data)
            # Committing class to database
            db.session.add(new_class)
            db.session.commit()

    navs = {'home': False, 'classes': True, 'logout': False, 'help': "classes"};

    return render_template("classes.html", form=form, classes=getClasses(), navs=navs)

@app.route("/delete_class/<id>", methods=['GET', 'POST'])
@login_required
def delete_class(id):
    db.session.delete(Classroom.query.filter_by(classId=id).first())
    db.session.commit()
    return redirect(url_for('classes'))

@app.route("/class_view/<id>", methods=['GET', 'POST'])
@login_required
def class_view(id):
    form = LectureForm()

    if form.validate_on_submit():
        if form.lecture_data.data != "":
            Lecture.query.filter_by(lectureId=form.lecture_data.data).update(dict(name=form.name.data, desc=form.description.data))
            db.session.commit()
        else:
            lid = str(uuid.uuid4())[:6]
            new_lecture = Lecture(lectureId=lid, classId=id, name=form.name.data, desc=form.description.data)
            db.session.add(new_lecture)
            db.session.commit()

    class_dict = Classroom.query.filter_by(classId=id).first().to_dict()
    navs = {'home': False, 'classes': False, 'class_view': 'live', 'help': 'class_view'};

    return render_template("classview.html", form=form, lectures=getLectures(id), classroom=class_dict, navs=navs)

@app.route("/delete_lecture/<id>", methods=['GET', 'POST'])
@login_required
def delete_lecture(id):
    lec = Lecture.query.filter_by(lectureId=id).first()
    cid = lec.classId
    db.session.delete(lec)
    db.session.commit()
    return redirect(url_for('class_view', id=cid))

@app.route("/inLecture", methods=['GET', 'POST'])
def lecture():
    lecture_id = str(request.form['lecture_id']);
    lecture = getLecture(lecture_id);
    topics = getTopics(lecture_id);
    navs = {'home': False, 'Lecture': "live", 'help': 'lecture'};

    return render_template("inLecture.html", lecture=lecture, topics=topics, navs=navs)


@app.route("/delete_topic/<id>", methods=['GET', 'POST'])
@login_required
def delete_topic(id):
    tpc = Topic.query.get(id);
    lid = tpc.lectureId;
    db.session.delete(tpc)
    db.session.commit()
    return redirect(url_for('lectureDetails', lecture_id=lid))

@app.route("/lectureDetails", methods=['GET', 'POST'])
@login_required
def lectureDetails():
    lecture_id = request.args.get('lecture_id');
    form = TopicForm();
    lectures = getLecture(lecture_id);
    clss = getClasse(lectures[0]['classId']);
    topics = getTopics(lecture_id);

    if form.validate_on_submit():
        if form.topic_data.data != "":
            t = Topic.query.filter_by(topicId=form.topic_data.data).first();
            t.name = form.name.data;
            t.topicDescription=form.description.data;
            db.session.commit();
        else:
            new_topic = Topic(lectureId=lecture_id, name=form.name.data, topicDescription=form.description.data)
            db.session.add(new_topic)
            db.session.commit()

    navs = {'home': False, 'Lecture': "live", 'help': 'lecture'};

    return render_template("lectureDetails.html",form=form, clss=clss, lecture=lectures, topics=topics, navs=navs);

@app.route("/help", methods=['GET'])
def help():
    navs = {'home': False, 'help': True};

    return render_template("help.html", navs=navs);

# Returns a list of dictionaries of the classrooms for the currently logged in user
def getClasses():
    returnList = []
    for c in Classroom.query.filter_by(owner=current_user.userId).all():
        returnList.append({'name' : c.name, 'desc' : c.desc, 'code' : c.classId})
    return returnList

#
def getClasse(id):
    returnList = []
    for c in Classroom.query.filter_by(classId=id).all():
        returnList.append({'name' : c.name, 'desc' : c.desc, 'owner' : c.owner})
    return returnList

# Returns a list of dictionaries of the Lectures for a certain classroom of the currently logged in user
def getLectures(id):
    returnList = []
    for l in Lecture.query.filter_by(classId=id).all():
        returnList.append({'id' : l.lectureId, 'name' : l.name, 'desc' : l.desc})
    return returnList

#
def getLecture(id):
    returnList = []
    for l in Lecture.query.filter_by(lectureId=id).all():
        returnList.append({'id' : l.lectureId, 'classId' : l.classId, 'name' : l.name, 'desc' : l.desc})
    return returnList

#
def getTopics(id):
    returnList = []
    for t in Topic.query.filter_by(lectureId=id).all():
        returnList.append({'id' : t.topicId, 'name' : t.name, 'topicDescription' : t.topicDescription, 'date' : t.date, 'ratingSum' : t.ratingSum, 'ratingTot' : t.ratingTot})
    return returnList

if __name__ == "__main__":
    socketio.run(app, debug=True)
