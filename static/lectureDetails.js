var socket = null;

$(document).ready(function() {
  socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

  // Printing connection
  socket.on('connect', function() {
    console.log("client connected");
  });

  // Printing any errors
  socket.on('connect_error', function(err) {
    console.log("client connect_error: ", err);
  });

  // Printing if connection timeout
  socket.on('connect_timeout', function(err) {
    console.log("client connect_timeout: ", err);
  });
});


function updateTopicId(id){
    document.forms[0].topic_data.value = id;
    bgc = 'bg-secondary'
    $('li').removeClass(bgc);
    $('#'+id).addClass(bgc);
    $('#editTopic').removeClass('disabled');
    $('#deleteTopic').removeClass('disabled');
    $('#resetTopic').removeClass('disabled');
    $('#deleteTopic').attr('href', 'delete_topic/'+id);
}

function clearForm(){
    document.forms[0].topic_data.value = "";
    $('li').removeClass(bgc);
    $('#editTopic').addClass('disabled');
    $('#deleteTopic').addClass('disabled');
    $('#resetTopic').addClass('disabled');
}

function resetTopicVotingStats() {
  if(socket != null) {
    id = document.forms[0].topic_data.value;
    socket.emit('topic_reset', {'id' : id});
    alert(id + ' Topic Voting Statistics Reset.');
  } else {
    console.log("Could not reset topic stats, there is no socket connection.");
  }
}
